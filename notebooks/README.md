### Installation

There are two files in this directory:

1. `requirements.txt`
1. `requirements.tensorflow-gpu.txt`

The first one is used for local development and testing.

The second one is used for converting the Jupyter Notebook to a Python
script used by GitLab CI.

```shell
pip3 install -r requirements.txt
```

Note that for Apple M1s, you have to replace the `tensorflow` package
with `tensorflow-macos`.
