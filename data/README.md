This directory contains public and private datasets for training a machine learning (ML) model to apply labels to issues.
The datasets are CSV files that need to be decompressed before processing.

The model trained on the private dataset is used in production for triaging [GitLab issues](https://gitlab.com/gitlab-org/gitlab/-/issues).

The public dataset is for education and development. You can use this repository to study how triaging with ML works, experiment with it, propose enhancements, and implement it in your own projects.

### Private training dataset (.7z)

`gitlab-issues.7z` is stored in Git LFS and encrypted with a password
in the CI/CD variables. It contains public and private issues with
already applied labels. To extract run:

```sh
7z x data/gitlab-issues.7z
```

This dataset is manually generated from the Rails console as described
in [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/294504).

### Public training dataset (.gz)

The public dataset is also manually collected using the "Export as CSV" button
and contains only a subset of issues, because CSV export is limited to
15 Mb. It also excludes private issues.

Use `gunzip` to unpack or stream CSV for processing or inspection. See the following example examining the CSV header:

```sh
$ gunzip < gitlab-org-gitlab_issues_2022-11-29.csv.gz | head -n 1
Title,Description,Issue ID,URL,State,Author,Author Username,Assignee,Assignee Username,Confidential,Locked,Due Date,Created At (UTC),Updated At (UTC),Closed At (UTC),Milestone,Weight,Labels,Time Estimate,Time Spent,Epic ID,Epic Title
```

To refresh the data.

1. Go to https://gitlab.com/gitlab-org/gitlab/-/issues and press export button

   ![image](manual-csv-export.png)

2. Wait for an email with the attached CSV. Note that it will contain the following statement about limits:

   > This attachment has been truncated to avoid exceeding the maximum allowed
   attachment size of 15 MB. 9411 of 45870 issues have been included. Consider
   re-exporting with a narrower selection of issues.
